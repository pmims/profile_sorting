require.config ({
	baseUrl: 'app',
	paths: {
		/* Modules */
		jquery:     '../node_modules/jquery/dist/jquery',
		underscore: '../node_modules/underscore/underscore',
		backbone:   '../node_modules/backbone/backbone',

		/* Models */
		model:          'js/models/model',
		candidateModel: 'js/models/candidateModel',
		individualModel: 'js/models/individualModel',

		/* Views */
		baseView:		'js/views/baseView',
		formView:       'js/views/formView',
		candidateView: 	'js/views/candidateView',
		individualView: 'js/views/individualView',
		collectionView: 'js/views/collectionView',

		/* Collections */
		collection:     'js/collections/collection',
	},
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		}
	}
});

define(['collectionView', 'collection'], 
	function(view, collection) {
		new view ({
			collection: new collection,
		});
	});
