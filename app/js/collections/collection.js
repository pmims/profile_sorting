define(['jquery', 'underscore', 'backbone', 'model'], 
    function($, _, Backbone, model) {
        var collection = Backbone.Collection.extend({
            initialize: function() {
                console.log("collection");
            },
            model: model,
            url: "https://randomuser.me/api/?results=16"
        });
        return collection;
    });
