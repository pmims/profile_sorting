define(['jquery', 'underscore', 'backbone', 'formView', 'candidateView'], 
    function($, _, Backbone, formView, candidateView) {
    var collectionView = Backbone.View.extend({
        initialize: function() {
            console.log("collectionView");
            this.fetch();
        },

        fetch: function() {
            this.listenTo(this.collection, "reset", this.render);
            this.collection.fetch({reset: true});
            this.render();
        },

        render: function() {
            this.collection.each(function(item) {
                this.renderModel(item);
            }, this);
        },

        renderModel: function(item) {
            new formView ({
                model: item
            });
            new candidateView ({
                model: item
            });
        }
    });
    return collectionView;
});
