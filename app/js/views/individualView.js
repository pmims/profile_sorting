define(['jquery', 'underscore', 'backbone', 'baseView'], 
    function($, _, Backbone, BaseView) {
        var individualView = BaseView.extend({
            initialize: function() {
                this.render();
            },
            el: "#individual",
            template: _.template($("#individual-template").html())
        });
        return individualView;
    });
