define(['jquery', 'underscore', 'backbone', 'baseView', 'candidateModel', 'candidateView'], 
    function($, _, Backbone, BaseView, candidateModel, candidateView) {
        var searchView = BaseView.extend({
            initialize: function() {
                console.log("searchView");

                var gender = $('input[name=gender]:checked').val();
                console.log("you chose this gender: ",  gender);
                this.render();
            },


            filter_gender: function() {
                var cModel;
                _.each(_.values(this.model.attributes.results), function(json) { 
                    if(json.gender === "female") { 
                        cModel = new candidateModel({ "results": json });
                    }
                });
                console.log("==>", cModel.attributes.results.gender);
                console.log("==>", cModel.attributes.results.name.first);
                console.log("==>", cModel.attributes.results.picture.large);
                console.log("==>", cModel.toJSON());

                var cView = new candidateView ({
                    model: cModel
                });
                cView.re_render();
            },

            el: "#search",
            template: _.template($("#search-template").html())
        });
        return searchView;
    });
