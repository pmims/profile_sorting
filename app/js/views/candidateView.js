define(['jquery', 'underscore', 'backbone', 'baseView', 'candidateModel', 'individualView', 'individualModel'], 
    function($, _, Backbone, BaseView, candidateModel, individualView, individualModel) {
        var candidateView = BaseView.extend({
            initialize: function() {
                this.render();
            },

            events: {
                'click #img_0, #img_1, #img_2, #img_3, #img_4, #img_5, #img_6, #img_7, #img_8, #img_9, #img_10, #img_11, #img_12, #img_13, #img_14, #img_15': 'individual_candidate',
            },

            individual_candidate: function(e) {
                let item = $(e.currentTarget).data('images');
                let json = Object.assign({}, this.model.attributes.results[item]);
                let model = new individualModel({"results": json});

                new individualView({ 
                    model: model
                });
            },

            el: "#candidate",
            template: _.template($("#candidate-template").html())
        });
        return candidateView;
    });
