define(['jquery', 'underscore', 'baseView', 'candidateModel', 'candidateView'], 
    function($, _, BaseView, candidateModel, candidateView) {
        var formView = BaseView.extend({
            initialize: function() {
                console.log("formView");
                this.render();
            },

            events: {
                'submit': 'formValue'
            },

            _filterByAny: function(m, f) {
                let gender = _.filter(this.model.attributes.results, function(json) {
                    if((json.gender == m) || (json.gender == f)) {
                        return json;
                    }
                });

                let model = new candidateModel({ "results": gender });

                new candidateView({
                    model: model 
                });
            },

            _filterByGender: function(gender) {
                return gender = _.filter(this.model.attributes.results, function(json) {
                    if(json.gender == gender) {
                        return json;
                    }
                });
            },

            _sortGenders: function(gender) {
                let json = this._filterByGender(gender);
                let model = new candidateModel({ "results": json });

                new candidateView({
                    model: model 
                });
            },

            formValue: function(event) {
                event.preventDefault();

                let _gender = $('input[name=gender]:checked').val();

                if(_gender === "any") { 
                    console.log(this.model.attributes.results);
                    this._filterByAny("male", "female");
                } else if(_gender === "male") {
                    this._sortGenders("male");
                } else {
                    this._sortGenders("female");
                }
            },

            el: "#form",
            template: _.template($("#form-template").html())
        });
        return formView;
    });
