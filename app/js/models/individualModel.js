define(['jquery', 'underscore', 'backbone'], 
    function($, _, Backbone) {
        var individualModel = Backbone.Model.extend({});
        return individualModel;
    });
