function ucase(name) {
    return name.charAt(0).toUpperCase() + name.substr(1);
}
